<h1>Projet de cours UQAC - Trust the Ball</h1>

<p><h2>Description du travail</h2>
Application de Réalité Virtuelle à deux personnes, où le but est de mettre une balle
dans un panier avec la manette tout en étant guidé par la personne ayant le casque.
</p>

<p><h2>Technologies et langages utilisés</h2>
<ul><li>Unity3D
    <li>Visual Studio
    <li>HTC Vive
    <li>C#
    <li>Git
</ul></p>

<p><h2>Méthode de travail</h2>
<ul><li>Méthodologie Agile : travail de groupe hebdomadaire
    <li>Pair-programming
</ul></p>

<p><h2>Rôles dans le projet</h2>
<ul><li>Game Designer
    <li>Programmeur
</ul></p>

<p><h2>Participants</h2>
<ul><li>Noémy Artigouha
    <li>Ludovic Jozereau
    <li>Florian Vidal
</ul></p>

[README Trust The Ball](https://gitlab.com/Jozereau_Ludovic/TrustTheBall/blob/master/TrustTheBall.pdf)

[Accès vers l'exécutable](https://drive.google.com/drive/folders/1XHtTR5jC654m7lwEamsyX4VTd4qZuNK-?usp=sharing)
